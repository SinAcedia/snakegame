// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"
#include "SnakeBase.h"
#include <math.h>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	ElementSize = 60.f;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::CreateFoodActor()
{
	FVector NewLocation;
	int x = rand() % 1000 - 500;
	int y = rand() % 1400 - 700;
	x = round(x / 60) * 60;
	y = round(y / 60) * 60;
	NewLocation = FVector(x, y, 50);
	FTransform NewTransform(NewLocation);
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			this->Destroy();
			this->CreateFoodActor();

		}
	}
}


