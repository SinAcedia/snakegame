// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "SnakeBase.h"
#include <math.h>


// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	ElementSize = 60.f;
	//TypeBonus = ETypeBonus::I;	
	int f = rand() % 2;
	TypeBonus = (ETypeBonus)f;
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ABonus::CreateBonusActor()
{
	FVector NewLocation;
	int x = rand() % 1000 - 500;
	int y = rand() % 1400 - 700;
	x = round(x / 60) * 60;
	y = round(y / 60) * 60;
	NewLocation = FVector(x, y, 50);
	FTransform NewTransform(NewLocation);

	BonusActor = GetWorld()->SpawnActor<ABonus>(BonusActorClass, NewTransform);
	
}

void ABonus::ChangeType()
{
	
}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			switch(TypeBonus)
			{
			case ETypeBonus::I:
				Snake->IncreaseMoveSpeed();
				break;
			case ETypeBonus::D:
				Snake->DecreaseMoveSpeed();
				break;
			}


			this->Destroy();
			this->CreateBonusActor();

		}
	}
}

ETypeBonus ABonus::GetTypeBonus()
{
	return TypeBonus;
}

bool ABonus::GetTypeBonusBool()
{
	if (TypeBonus == ETypeBonus::I)
		return true;
	else
		return false;
}


