// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Bonus.generated.h"

UENUM()
enum class ETypeBonus
{
	I,
	D
};
UCLASS()
class SNAKEGAME_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(BlueprintReadWrite)
		ABonus* BonusActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusActorClass;

	UPROPERTY(EditDefaultsOnly)
		ETypeBonus TypeBonus;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
	void ChangeType();
	
	void CreateBonusActor();

	UFUNCTION(BlueprintCallable)
		ETypeBonus GetTypeBonus();
	UFUNCTION(BlueprintCallable)
		bool GetTypeBonusBool();
};
