// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Bonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonus() {}
// Cross Module References
	SNAKEGAME_API UEnum* Z_Construct_UEnum_SnakeGame_ETypeBonus();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonus_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	static UEnum* ETypeBonus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SnakeGame_ETypeBonus, Z_Construct_UPackage__Script_SnakeGame(), TEXT("ETypeBonus"));
		}
		return Singleton;
	}
	template<> SNAKEGAME_API UEnum* StaticEnum<ETypeBonus>()
	{
		return ETypeBonus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETypeBonus(ETypeBonus_StaticEnum, TEXT("/Script/SnakeGame"), TEXT("ETypeBonus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SnakeGame_ETypeBonus_Hash() { return 1748157498U; }
	UEnum* Z_Construct_UEnum_SnakeGame_ETypeBonus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SnakeGame();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETypeBonus"), 0, Get_Z_Construct_UEnum_SnakeGame_ETypeBonus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETypeBonus::I", (int64)ETypeBonus::I },
				{ "ETypeBonus::D", (int64)ETypeBonus::D },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "D.Name", "ETypeBonus::D" },
				{ "I.Name", "ETypeBonus::I" },
				{ "ModuleRelativePath", "Bonus.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SnakeGame,
				nullptr,
				"ETypeBonus",
				"ETypeBonus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(ABonus::execGetTypeBonusBool)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetTypeBonusBool();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABonus::execGetTypeBonus)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ETypeBonus*)Z_Param__Result=P_THIS->GetTypeBonus();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABonus::execChangeType)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ChangeType();
		P_NATIVE_END;
	}
	void ABonus::StaticRegisterNativesABonus()
	{
		UClass* Class = ABonus::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ChangeType", &ABonus::execChangeType },
			{ "GetTypeBonus", &ABonus::execGetTypeBonus },
			{ "GetTypeBonusBool", &ABonus::execGetTypeBonusBool },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABonus_ChangeType_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonus_ChangeType_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonus_ChangeType_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonus, nullptr, "ChangeType", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonus_ChangeType_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonus_ChangeType_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonus_ChangeType()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonus_ChangeType_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABonus_GetTypeBonus_Statics
	{
		struct Bonus_eventGetTypeBonus_Parms
		{
			ETypeBonus ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Bonus_eventGetTypeBonus_Parms, ReturnValue), Z_Construct_UEnum_SnakeGame_ETypeBonus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonus, nullptr, "GetTypeBonus", nullptr, nullptr, sizeof(Bonus_eventGetTypeBonus_Parms), Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonus_GetTypeBonus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonus_GetTypeBonus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics
	{
		struct Bonus_eventGetTypeBonusBool_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((Bonus_eventGetTypeBonusBool_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Bonus_eventGetTypeBonusBool_Parms), &Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonus, nullptr, "GetTypeBonusBool", nullptr, nullptr, sizeof(Bonus_eventGetTypeBonusBool_Parms), Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonus_GetTypeBonusBool()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonus_GetTypeBonusBool_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABonus_NoRegister()
	{
		return ABonus::StaticClass();
	}
	struct Z_Construct_UClass_ABonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TypeBonus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TypeBonus;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TypeBonus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_BonusActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BonusActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElementSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ElementSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABonus_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABonus_ChangeType, "ChangeType" }, // 2520021295
		{ &Z_Construct_UFunction_ABonus_GetTypeBonus, "GetTypeBonus" }, // 3652102917
		{ &Z_Construct_UFunction_ABonus_GetTypeBonusBool, "GetTypeBonusBool" }, // 47670214
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Bonus.h" },
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus = { "TypeBonus", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonus, TypeBonus), Z_Construct_UEnum_SnakeGame_ETypeBonus, METADATA_PARAMS(Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonus_Statics::NewProp_BonusActorClass_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ABonus_Statics::NewProp_BonusActorClass = { "BonusActorClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonus, BonusActorClass), Z_Construct_UClass_ABonus_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ABonus_Statics::NewProp_BonusActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::NewProp_BonusActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonus_Statics::NewProp_BonusActor_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABonus_Statics::NewProp_BonusActor = { "BonusActor", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonus, BonusActor), Z_Construct_UClass_ABonus_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABonus_Statics::NewProp_BonusActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::NewProp_BonusActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonus_Statics::NewProp_ElementSize_MetaData[] = {
		{ "Category", "Bonus" },
		{ "ModuleRelativePath", "Bonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABonus_Statics::NewProp_ElementSize = { "ElementSize", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonus, ElementSize), METADATA_PARAMS(Z_Construct_UClass_ABonus_Statics::NewProp_ElementSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::NewProp_ElementSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonus_Statics::NewProp_TypeBonus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonus_Statics::NewProp_BonusActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonus_Statics::NewProp_BonusActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonus_Statics::NewProp_ElementSize,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonus_Statics::ClassParams = {
		&ABonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABonus_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonus, 2293880484);
	template<> SNAKEGAME_API UClass* StaticClass<ABonus>()
	{
		return ABonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonus(Z_Construct_UClass_ABonus, &ABonus::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
